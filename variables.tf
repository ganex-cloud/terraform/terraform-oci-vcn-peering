variable "type" {
  description = "(Required) Type of VCN Peering. Accepted values are local and remote"
  type        = string
}

variable "compartment_id" {
  description = "(Required) (Updatable) The OCID of the compartment containing the local peering gateway (LPG)."
  type        = string
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = null
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = null
}

variable "display_name" {
  description = "(Optional) (Updatable) A user-friendly name. "
  type        = string
  default     = ""
}

variable "peer_id" {
  description = "(Optional) The OCID of the LPG you want to peer with."
  type        = string
  default     = null
}

# Local VCN Peering
variable "route_table_id" {
  description = "(Optional) (Updatable) The OCID of the route table the LPG will use."
  type        = string
  default     = null
}

variable "vcn_id" {
  description = "(Optional) Required if type is local. The OCID of the VCN the LPG belongs to."
  type        = string
  default     = null
}

# Remote VCN Peering
variable "drg_id" {
  description = "(Optional) Required if type is remote. The OCID of the DRG the RPC belongs to."
  type        = string
  default     = null
}

variable "peer_region_name" {
  description = "(Optional) Required if type is remote. The name of the region that contains the RPC you want to peer with."
  type        = string
  default     = null
}
