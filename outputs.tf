output "peer_id" {
  description = "The LPG's Oracle ID (OCID)."
  value       = var.type == "local" ? oci_core_local_peering_gateway.this[0].id : oci_core_remote_peering_connection.this[0].id
}
