resource "oci_core_local_peering_gateway" "this" {
  count          = var.type == "local" ? 1 : 0
  compartment_id = var.compartment_id
  vcn_id         = var.vcn_id
  defined_tags   = var.defined_tags
  display_name   = var.display_name
  freeform_tags  = var.freeform_tags
  peer_id        = var.peer_id
  route_table_id = var.route_table_id
}

resource "oci_core_remote_peering_connection" "this" {
  count            = var.type == "remote" ? 1 : 0
  compartment_id   = var.compartment_id
  drg_id           = var.drg_id
  defined_tags     = var.defined_tags
  display_name     = var.display_name
  freeform_tags    = var.freeform_tags
  peer_id          = var.peer_id
  peer_region_name = var.peer_region_name
}
