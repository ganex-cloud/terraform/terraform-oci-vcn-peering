# VCN vcn-1
module "vcn_peering-vcn-1" {
  #source          = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-vcn.git?ref=master"
  source         = "/mnt//dados//git//ganex/public/terraform/terraform-oci-vcn-peering/"
  compartment_id = module.iam_compartment-vcn-2.compartment_id
  type           = "local"
  vcn_id         = module.vcn-vcn-2.vcn_id
  display_name   = "vcn-1"
  peer_id        = data.terraform_remote_state.oci-vcn-1.outputs.vcn-peering-vcn-2_peer_id
}

# VCN vcn-2 (Create this resource first)
module "vcn_peering-vcn-2" {
  #source          = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-vcn.git?ref=master"
  source         = "/mnt//dados//git//ganex/public/terraform/terraform-oci-vcn-peering/"
  compartment_id = data.terraform_remote_state.oci-vcn-2.outputs.vcn-1_compartment_id
  type           = "local"
  vcn_id         = module.vcn-vcn-1.vcn_id
  display_name   = "vcn-2"
}
